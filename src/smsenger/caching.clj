(ns smsenger.caching
  (:require [taoensso.carmine :as redis]))

(def redis-conn {:pool {} :spec {}})
(defmacro with-conn [& body] `(redis/wcar redis-conn ~@body))

(defn fetch[k]
  (with-conn (redis/get k)))

(defn store[k v]
  (with-conn (redis/set k v)))
