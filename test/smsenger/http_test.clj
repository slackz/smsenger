(ns smsenger.http-test
  (:use clojure.test
        smsenger.http))

(deftest test-wiki-url
  (testing "constructing the URL for one term"
    (is (= "https://api.duckduckgo.com/?q=wikipedia+fudge&format=json"
           (wiki-url "fudge"))))
  (testing "constructing the URL for multiple terms"
    (is (= "https://api.duckduckgo.com/?q=wikipedia+fudge+cookie&format=json"
           (wiki-url "fudge cookie")))))
